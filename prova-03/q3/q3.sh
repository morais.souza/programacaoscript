vida_jogador=$1
vida_dragao=$2


nome_dragao="Fafnir"

while true; do
    echo "====================="
    echo "Jogo do Dragão"
    echo "====================="
    echo "Opções:"
    echo "1. Atacar"
    echo "2. Fugir"
    echo "3. Info"
    echo "====================="
    read -p "Escolha uma opção: " opcao

    case $opcao in
        1)  echo "Você atacou o dragão causando 100 pontos de dano!"
            vida_dragao=$((vida_dragao - 100))
            echo "O dragão contra-atacou e te causou 10 pontos de dano!"
            vida_jogador=$((vida_jogador - 10))
            if [ $vida_dragao -le 0 ]; then
                 echo "Você derrotou o dragão $nome_dragao! Parabéns, você venceu!"
                 exit 0
            fi
            ;;

        2)  echo "Você tentou fugir do dragão."
            chance=$((RANDOM % 2))
            if [ $chance -eq 0 ]; then
                echo "Você conseguiu fugir com sucesso! O jogo terminou."
                exit 0
            else
                echo "O dragão soltou uma baforada de fogo e você foi derrotado!"
                exit 1
            fi
            ;;

        3)  echo "Sua vida: $vida_jogador"
            echo "Vida do dragão $nome_dragao: $vida_dragao"
            ;;

        *)  echo "Digite direito meu filho."
            ;;
    esac
done
