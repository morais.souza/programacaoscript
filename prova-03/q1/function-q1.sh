function menu() {
        echo "---------------------------------------------------"
        echo "1 -> Verifica se o usuário existe;"
        echo "2 -> Verifica se o usuário está logado na máquina"
        echo "3 -> LIsta os arquivos da pasta home do ussuário;"
        echo "4 -> Sair;"
        read op
        echo "---------------------------------------------------"
}

function opc1() {
        read -p "Digite o nome do usuário: " user1
        if id "$user1" >/dev/null 2>&1; then
                echo "O usuário $user1 existe."
        else 
                echo "O usuário $user1 não existe"
        fi
}

function opc2() {
        read -p "Digite o nome do usuário: " user2
        if who | grep -wq "$user2"; then
                echo "O usuário $user2 está logado."
        else
                echo "O usuário $user2 não está logado."
        fi
}

function opc3() {
        read -p "Digite o nome do usuário: " user3
        home="/home/$user3"

        if [ -d "$home" ]; then
                ls -l "$home"
        else
                echo "A pasta home do $user3 não foi encontrada"
        fi
}


