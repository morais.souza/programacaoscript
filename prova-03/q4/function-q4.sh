function gerar_numero() {
        min=$1
        max=$2
        echo $((RANDOM % (max - min +1) + min))
}

function menu() {
        echo "====================="
        echo "Jogo do Dragão"
        echo "====================="
        echo "Opções:"
        echo "1. Atacar"
        echo "2. Fugir"
        echo "3. Curar"
        echo "4. Info"
        echo "====================="
        read -p "Escolha uma opção: " opcao

}

function opc1() {
        dano_jogador=$(gerar_numero 10 100)
        vida_dragao=$((vida_dragao - vida_jogador))

        echo "Você atacou o dragão causando $dano_jogador pontos de dano!"
        ataques_dragao=$(gerar_numero 1 5)
        dano_total_dragao=0

        for ((i = 1; i <= ataques_dragao; i++)); do
                dano_dragao=$(gerar_numero 1 10)
                dano_total_dragao=$((dano_total_dragao + dano_dragao))
        done

        vida_jogador=$((vida_jogador - dano_total_dragao))

        echo "O dragão contra-atacou com $ataques_dragao ataques, causando $dano_total_dragao pontos de dano"

        if [ $vida_dragao -le 0 ]; then
               echo "Você derrotou o dragão $nome_dragao! Parabéns, você venceu!"
               exit 0
        fi

}

function opc2() {
        chance_fugir=$(gerar_numero 1 5)

        if [ $chance_fugir -eq 1 ]; then
                echo "Você conseguiu fugir com sucesso! O jogo terminou."
                exit 0
        else
                echo "O dragão soltou uma baforada de fogo e você foi derrotado!"
                exit 1
        fi
}

function opc3() {
        cura=$(gerar_numero 50 $((200 - vida_inicial_jogador)))
        vida_jogador=$((vida_jogador + cura))

        echo "Você se curou e recuperou $cura pontos de vida!"
        chance_fugir_dragao=$(gerar_numero 1 10)

        if [ $chance_fugir_dragao -eq 1 ]; then
                echo "O dragão sumiu do mapa, logo você pensa que ele fugiu de você."
                exit 0
        fi
}

function opc4() {
        echo "Sua vida: $vida_jogador"
        echo "Vida do dragão $nome_dragao: $vida_dragao"
}

