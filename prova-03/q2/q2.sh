rquivo_lista="lista.txt"
arquivo_resultado="resultado.txt"

rm -f "$arquivo_resultado"

while IFS= read -r arquivo; do
    if [ -f "$arquivo" ]; then
        hash=$(md5sum "$arquivo" | awk '{print $1}')
        echo "$arquivo: $hash" >> "$arquivo_resultado"
    else
        echo "O arquivo '$arquivo' não foi encontrado."
    fi
done < "$arquivo_lista"

echo "Processamento concluído. Os resultados foram salvos no arquivo '$arquivo_resultado'."
