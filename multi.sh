if [ $# -ne 2 ]; then 
	echo "Uso: $0 a b"
	exit 1
fi

a=$1
b=$2

resultado=$(( (a+1) * (b+1)))

echo "O valor da expressão (a+1) * (b+1) é $resultado."
