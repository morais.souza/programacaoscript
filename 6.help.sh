echo "Substituições de Variáveis: "
echo "O usuário atual é: $USER"
echo "O diretório atual é: $PWD"
echo "O nome do script é: $0"
echo ""

echo "Sbustituições de Shell: "
echo "Os arquivos no diretório atual são: "
echo "$(ls)"
echo ""

echo "Substituição Aritimética: "
echo "A soma de 2 + 2 é: $((2+2))"
echo "O valor de PI com duas casas decimais é: $(echo "scale=2; 4*a(1)" | bc -l)"

