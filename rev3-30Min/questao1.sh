#!/bin/bash


if [ $# -ne 1 ]; then
  echo "Uso: $0 <arquivo>"
  exit 1
fi


if [ ! -f "$1" ]; then
  echo "O arquivo '$1' não existe."
  exit 1
fi


echo "Menu:"
echo "1. Exibir as 10 primeiras linhas do arquivo"
echo "2. Exibir as 10 últimas linhas do arquivo"
echo "3. Exibir o número de linhas do arquivo"
echo -n "Escolha uma opção (1-3): "

read opcao

if [ "$opcao" -eq 1 ]; then
  echo "As 10 primeiras linhas do arquivo $1 são:"
  head -n 10 "$1"
elif [ "$opcao" -eq 2 ]; then
  echo "As 10 últimas linhas do arquivo $1 são:"
  tail -n 10 "$1"
elif [ "$opcao" -eq 3 ]; then
  echo "O número de linhas do arquivo $1 é:"
  wc -l < "$1"
else
  echo "Opção inválida."
fi
