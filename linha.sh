if [ $# -ne 2 ]; then 
	echo "Uso: $0 arquivo1 arquivo2"
	exit 1
fi

arquivo1=$1
arquivo2=$2

linhas_arquivo1=$(wc -l < "$arquivo1")
linhas_arquivo2=$(wc -l < "$arquivo2")
soma=$(( linhas_arquivo1 + linhas_arquivo2 ))

echo "A soma de linhas dos arquivos $arquivo1 e $arquivo2 é $soma."
