#!/bin/bash

prox_quarta=$(date -d "next wednesday" +%Y-%m-%d)

echo "Quartas-feiras das próximas 3 semanas:"
echo "$(date -d $prox_quarta +%d/%m/%Y)"
echo "$(date -d "next wednesday + 1 week" +%d/%m/%Y)"
echo "$(date -d "next wednesday + 2 weeks" +%d/%m/%Y)"
echo "$(date -d "next wednesday + 3 weeks" +%d/%m/%Y)"

mkdir -p backup-$(date -d "$prox_quarta" +%d-%m-%Y)
mkdir -p backup-$(date -d "$prox_quarta + 1 week" +%d-%m-%Y)
mkdir -p backup-$(date -d "$prox_quarta + 2 week" +%d-%m-%Y)
mkdir -p backup-$(date -d "$prox_quarta + 3 week" +%d-%m-%Y)
