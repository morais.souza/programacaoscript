#!/bin/bash

echo "Digite a operação que deseja realizar (+, -, *, /, sqrt):"
read op

echo "Digite o primeiro número:"
read num1

echo "Digite o segundo número: (Caso selecionado a função sqrt desconsidere essa mensagem)"
read num2

[ "$op" = "+" ] && echo "$num1 + $num2 = $(echo "$num1 + $num2" | bc)" && exit
[ "$op" = "-" ] && echo "$num1 - $num2 = $(echo "$num1 - $num2" | bc)" && exit
[ "$op" = "*" ] && echo "$num1 * $num2 = $(echo "$num1 * $num2" | bc)" && exit
[ "$op" = "/" ] && echo "$num1 / $num2 = $(echo "scale=2; $num1 / $num2" | bc)" && exit
[ "$op" = "sqrt" ] && echo "sqrt($num1) = $(echo "scale=2; sqrt($num1)" | bc)" && exit

