echo "Escolha uma opção para customizar o prompt:"
echo "1- Adicionar data e hora"
echo "2- Adicionar nome de usuário"
echo "3- Adicionar diretório atual"
echo "4- Adicionar cor ao prompt"
echo "5- Retornar para o prompt original"

read opcao

(( opcao == 1 )) && PS1="\d \t $ " && echo "Data e hora adicionado."
(( opcao == 2 )) && PS1="\u $ " && echo "Nome do Usuário adicionado."
(( opcao == 3 )) && PS1="\w $ " && echo "Diretório atual adicionado."
(( opcao == 4 )) && PS1="\[\e[1;33m\]\u@\h:\w\$\[\e[0m\] " && echo "Cor adicionado."
(( opcao == 5 )) && PS1="\u@\h:\w\$ " && echo "Retornando para o prompt original."

