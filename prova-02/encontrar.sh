#!/bin/bash

arq=$1

[ -e "$arq" ] && echo "Arquivo encontrado no diretório atual." && exit
[ -e "/tmp/$arq" ] && echo "Arquivo encontrado no diretório /tmp." && exit
[ -e "/etc/$arq" ] && echo "Arquivo encontrado no diretório /etc." && exit

echo "Arquivo não encontrado em nenhum dos diretórios especificados."
