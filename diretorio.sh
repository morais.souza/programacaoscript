if [ "$#" -ne 3 ]; then
 	echo "Por favor, informe o nome de  três diretórios: "
	exit 1
fi
dir1=$1
dir2=$2
dir3=$3

if [ ! -d "$dir1" ]; then
	echo "Diretório $dir1 não encontrado"
	exit 1
fi

if [ ! -d "$dir2" ]; then
	echo "Diretório $dir2 não encontrado"
	exit 1
fi

if [ ! -d "$dir3" ]; then
	echo "Diretório $dir3 não encontrado"
	exit 1
fi

echo "Arquivos do diretório $dir1: " 
ls $dir1
read -p "Pressione ENTER para continuar"
echo "Arquivos do diretório $dir2: "
ls $dir2
read -p "Pressione ENTER para continuar"
echo "Arquivos do diretório $dir3: "
ls $dir3
read -p "Pressione ENTER para sair"

