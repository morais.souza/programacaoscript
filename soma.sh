if [ $# -ne 2 ]; then 
	echo "Uso: $0 <número1> <número2>"
	exit 1
fi

x=$1
y=$2

resultado=$((x + y))

echo "A soma de $x e $y é $resultado."
