echo "Digite o nome de 3 diretórios:"
read dir1 dir2 dir3

echo "Quantidade de arquivos com extensão .txt:"
ls $dir1/*.txt $dir2/*.txt $dir3/*.txt 2>/dev/null | wc -l

echo "Quantidade de arquivos com extensão .png:"
ls $dir1/*.png $dir2/*.png $dir3/*.png 2>/dev/null | wc -l

echo "Quantidade de arquivos com extensão .doc:"
ls $dir1/*.doc $dir2/*.doc $dir3/*.doc 2>/dev/null | wc -l
