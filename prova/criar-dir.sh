if [ $# -ne 4 ]; then
  echo "Número incorreto de parâmetros"
  exit 1
fi


for dir in "$@"; do
  mkdir "$dir"
  echo "Diretório $dir criado."

  echo "$dir" > "$dir/README.md"
done

echo "Concluído com Sucesso!"
