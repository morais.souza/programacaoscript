arquivo1=$1
arquivo2=$2
arquivo3=$3

linhas_arquivo1=$(wc -l < "$arquivo1")
linhas_arquivo2=$(wc -l < "$arquivo2")
linhas_arquivo3=$(wc -l < "$arquivo3")


soma=$(expr $linhas_arquivo1 + $linhas_arquivo2 + $linhas_arquivo3)


echo "A soma dos números de linhas: $soma"
